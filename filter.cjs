function filter(elements, cb){
    const filtered = [];
    let index=0;
    for(const ele of elements){
        if (cb(ele,index, elements ) === true){
            filtered.push(ele)
        }
        index+=1;
    }
    return filtered;
}

module.exports = filter;