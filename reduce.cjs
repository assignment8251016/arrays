function reduce(elements, cb, accumulator){
    let index = 0;
    console.log("initial value:",accumulator)
    for(const ele of elements){
        
        if (accumulator === undefined){
            accumulator = elements[0]
            index+=1
            continue
        }
        

        accumulator = cb(accumulator, ele, index, elements);
        index+=1;
    }

    return accumulator;
}
module.exports = reduce