function find(elements, cb){
    for(const ele of elements){
        if(cb(ele)){
            return ele
        }
    }
    return undefined
}

module.exports = find